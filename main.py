#!/usr/bin/env python3

"""
https://blog.paperspace.com/beginners-guide-to-boltzmann-machines-pytorch/
"""

#import random
#import time

import imageio
import matplotlib.pyplot as plt
import matplotlib.animation as ani
import numpy as np
import torch
import torchvision

print ("PyTorch version:", torch.__version__)

class RBM (torch.nn.Module):
  def __init__(self, num_visible=28 * 28, num_hidden = 500, k = 5):
    super (RBM, self).__init__()
    self.weights      = torch.nn.Parameter (
      torch.randn (num_hidden, num_visible) * 0.001)
    self.bias_visible = torch.nn.Parameter (torch.zeros (num_visible))
    self.bias_hidden  = torch.nn.Parameter (torch.zeros (num_hidden))
    self.k            = k

  def forward (self, visible):
    pre_h1, h1 = self.visible_to_hidden (visible)
    h_ = h1
    for _ in range (self.k):
      pre_v_, v_ = self.hidden_to_visible (h_)
      pre_h_, h_ = self.visible_to_hidden (v_)
    return visible, v_

  def free_energy (self, visible):
    vbias_term  = visible.mv (self.bias_visible)
    wx_b        = torch.nn.functional.linear (visible, self.weights,
      self.bias_hidden)
    hidden_term = wx_b.exp().add (1).log().sum (1)
    return (-hidden_term - vbias_term).mean()

  def visible_to_hidden (self, visible):
    p_h = torch.sigmoid (
      torch.nn.functional.linear (visible, self.weights, self.bias_hidden))
    sample_h = self.sample_from_p (p_h)
    return p_h, sample_h
  def hidden_to_visible (self, hidden):
    p_v = torch.sigmoid (
      torch.nn.functional.linear (hidden, self.weights.t(), self.bias_visible))
    sample_v = self.sample_from_p (p_v)
    return p_v, sample_v

  def sample_from_p (self, p):
    return torch.nn.functional.relu (
      torch.sign (p - torch.autograd.Variable (torch.rand (p.size())))
    )

def show_and_save (file_name, image):
  npimage = np.transpose (image.numpy(), (1, 2, 0))
  f = "./%s.png" % file_name
  plt.imshow (npimage)
  plt.imsave (f, npimage)

batch_size   = 64
train_loader = torch.utils.data.DataLoader (
  torchvision.datasets.MNIST (
    "./data",
    train=True,
    download=True,
    transform=torchvision.transforms.Compose (
      [torchvision.transforms.ToTensor()]
    )
  ),
  batch_size=batch_size
)
test_loader = torch.utils.data.DataLoader (
  torchvision.datasets.MNIST (
    "./data",
    train=False,
    transform=torchvision.transforms.Compose (
      [torchvision.transforms.ToTensor()]
    )
  ),
  batch_size=batch_size
)

rbm = RBM (k=1)
#FIXME
print ("WEIGHTS:\n", rbm.weights)

"""
tensor = torch.tensor(())
random = tensor.new_full ((1, 784), 0.5).bernoulli()
v, v1 = rbm (random)
print ("V1 SIZE:", v1.size())
npimage = v1.view((28, 28)).detach().numpy()
f = "sample.png"
plt.imsave (f, npimage)
npimage = random.view((28, 28)).numpy()
f = "random.png"
plt.imsave (f, npimage)
exit (1)
"""

"""
figure, ax = plt.subplots()
images = []
tensor = torch.tensor(())
v1 = tensor.new_full ((1, 784), 0.5).bernoulli()
for _ in range (100):
  v, v1 = rbm (v1)
  npimage = v1.view((28, 28)).detach().numpy()
  image = ax.imshow (npimage, cmap="gray")
  images.append ([image])
animation = ani.ArtistAnimation (figure, images, interval=50, blit=True,
  repeat_delay=1000)
plt.show()
exit (1)
"""

"""
# image animation test
print ("DATA DIM:", data.dim())
print ("DATA SIZE:", data.size())
figure, ax = plt.subplots()
images = []
for i in range (64):
  #print ("DATA[i][0] SIZE:", data[i][0].size())
  npimage = data[i][0].numpy()   # [28, 28]
  #print ("IMAGE:", npimage)
  print ("IMAGE SIZE:", npimage.size)
  image = ax.imshow (npimage, cmap="gray")
  images.append ([image])
animation = ani.ArtistAnimation (figure, images, interval=50, blit=True,
  repeat_delay=1000)
plt.show()
exit (1)
#
"""

"""
train_op = torch.optim.SGD (rbm.parameters(), 0.1)

for epoch in range (10):
  losses = []
  for data, target in iter (train_loader):
    data        = torch.autograd.Variable (data.view (-1, 784))
    # image animation test goes here
    sample_data = data.bernoulli()
    v, v1       = rbm (sample_data)
    loss        = rbm.free_energy (v) - rbm.free_energy (v1)
    losses.append (loss.data)
    train_op.zero_grad()
    loss.backward()
    train_op.step()

  print ("Training loss for {} epoch: {}".format (epoch, np.mean (losses)))

torch.save (rbm.state_dict(), "rbm.bin")
"""

rbm.load_state_dict (torch.load ("rbm.bin"))
rbm.eval()

"""
for data, target in iter (train_loader):
  data = torch.Tensor (data[1][0].view (-1, 784))
  break
npimage = data.view((28, 28)).numpy()
f = "data2.png"
plt.imsave (f, npimage)
"""
npimage = imageio.v3.imread ("image.png")
npimage = npimage.transpose ((2, 0, 1))[0]
npimage = npimage / 255
print ("IMAGE SIZE:", npimage.size)
print ("IMAGE SHAPE:", npimage.shape)
print ("IMAGE DTYPE:", npimage.dtype)
data = torch.Tensor (npimage).view (-1, 784)

figure, ax = plt.subplots()
images = []
tensor = torch.tensor(())
v1 = data.bernoulli()
for _ in range (100):
  v, v1 = rbm (v1)
  npimage = v1.view((28, 28)).detach().numpy()
  image = ax.imshow (npimage, cmap="gray")
  images.append ([image])
animation = ani.ArtistAnimation (figure, images, interval=50, blit=True,
  repeat_delay=1000)
plt.show()
exit (1)

show_and_save ("real", torchvision.utils.make_grid (v.view (32, 1, 28, 28).data))
