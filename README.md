# `boltztut`

> Restricted Boltzmann machine tutorial in PyTorch

<https://blog.paperspace.com/beginners-guide-to-boltzmann-machines-pytorch/>

```
$ nix-shell
$ pipenv shell
$ pipenv install
$ ./main.py
```
